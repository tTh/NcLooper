/*
 *      NcLooper        fonctions audio output
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <string.h>
#include  <fcntl.h>
#include  <math.h>

#include <ao/ao.h>
#include <sndfile.h>

#include  "ao_output.h"

/* --------------------------------------------------------------------- */

extern int		verbosity;

/* --------------------------------------------------------------------- */
ao_device *init_ao_output(int smplrate)
{
int			default_driver;
ao_sample_format	format;
ao_device		*device;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, smplrate);
#endif

ao_initialize();

default_driver = ao_default_driver_id();
#if DEBUG_LEVEL
fprintf(stderr, "%s : ao default driver #%d\n", __func__, default_driver);
#endif

memset(&format, 0, sizeof(format));
format.bits = 16;
format.channels = 2;
format.rate = smplrate;
format.byte_format = AO_FMT_LITTLE;

device = ao_open_live(default_driver, &format, NULL);
if (device == NULL) {
        fprintf(stderr, "Error opening AO device.\n");
        return NULL;
        }

#if DEBUG_LEVEL
fprintf(stderr, "%s : device at %p\n", __func__, device);
#endif

return device;
}
/* --------------------------------------------------------------------- */
int close_ao_output(ao_device *dev)
{

if (NULL == dev) {
	fprintf(stderr, "%s : no output to close\n", __func__);
	return -1;
	}

ao_close(dev);
ao_shutdown();

return 0;
}
/* --------------------------------------------------------------------- */
int play_some_stuff(ao_device *dev, int notused)
{
int			i, sample;
char			*buffer;
float			freq = 440.0;

#define NB_SAMPLES	(44100*2)

if (NULL == dev) {
	fprintf(stderr, "%s : please call 'init_ao_output' first\n",
					__func__);
	exit(1);
	}

buffer = calloc(NB_SAMPLES*2, sizeof(short));

for (i = 0; i < NB_SAMPLES; i++) {
	sample = (int)(0.75 * 32768.0 *
		sin(2 * M_PI * freq * ((float) i/44100)));

	/* Put the same stuff in left and right channel */
	buffer[4*i] = buffer[4*i+2] = sample & 0xff;
	buffer[4*i+1] = buffer[4*i+3] = (sample >> 8) & 0xff;
	}
ao_play(dev, buffer, NB_SAMPLES);

free(buffer);

return 0;
}
/* --------------------------------------------------------------------- */

/* --------------------------------------------------------------------- */

