/*
 *      NcLooper        fonctions audio output
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <string.h>
#include  <fcntl.h>
#include  <math.h>

#include <ao/ao.h>
#include <sndfile.h>

#include  "ao_output.h"

/* --------------------------------------------------------------------- */

extern int			verbosity;

#define		T_BUFF_WAVES	(16384*2)

/* --------------------------------------------------------------------- */

int blast_this_file(char *fname, ao_device *dev, int loop)
{
SNDFILE                 *sndf;
SF_INFO                 sfinfo;
int                     foo, lus, pass;
short			*buffer;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p %d )\n", __func__, fname, dev, loop);
#endif

buffer = calloc(T_BUFF_WAVES, sizeof(short)*2);
if (NULL == buffer) {
	fprintf(stderr, "ALLOC FAIL, ABEND in %s\n", __func__);
	abort();
	}

sndf = sf_open(fname, SFM_READ, &sfinfo);
if (NULL==sndf)
	{
	perror("sf_open");
	abort();
	}

buffer = calloc(T_BUFF_WAVES, sizeof(short)*2);

if (verbosity) {
	fprintf(stderr, "samplerate : %d\n", sfinfo.samplerate);
	fprintf(stderr, "frames     : %ld\n", sfinfo.frames);
	fprintf(stderr, "seekable   : %d\n", sfinfo.seekable);
	}

for (pass=0; pass<loop; pass++) {

	sf_seek(sndf, 0, SEEK_SET);

	while ((lus = sf_read_short(sndf, buffer, T_BUFF_WAVES))) {

#if DEBUG_LEVEL > 1
		fprintf(stderr, "%s : %d bytes read\n", fname, lus);
#endif
		ao_play(dev, buffer, lus*2);
		}

	}

/* do some cleanup */
sf_close(sndf);
free(buffer);

return 0;
}


/* --------------------------------------------------------------------- */
