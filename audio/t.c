/*
 *      NcLooper        test des fonctions audio
 */
 
#include  <stdio.h>
#include <unistd.h>

#include <ao/ao.h>

#include  "ao_output.h"
#include  "playfile.h"

/* --------------------------------------------------------------------- */

int			verbosity;

/* --------------------------------------------------------------------- */
void help(int k)
{
}
/* --------------------------------------------------------------------- */
int envoyer_du_son(char *fname, int k)
{
ao_device		*device;
int			foo;

device = init_ao_output(44100);
if (verbosity) fprintf(stderr, "AO init    -> %p\n", device);

switch (k) {
	case 0:
		foo = blast_this_file(fname, device, 1);
		fprintf(stderr, "blast file -> %d\n", foo);
		break;

	case 1:
		foo = play_some_stuff(device, 0);
		fprintf(stderr, "play stuff -> %d\n", foo);
		break;
	}

foo = close_ao_output(device);
if (verbosity) fprintf(stderr, "AO close   -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int			opt;

while ((opt = getopt(argc, argv, "hv")) != -1) {
	switch(opt) {
		case 'h':	help(0);			break;
		case 'v':	verbosity++;			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc = %d, optind = %d\n", argc, optind);
#endif

envoyer_du_son("AK14V-ORDRES.wav", 0);

return 0;
}
/* --------------------------------------------------------------------- */
