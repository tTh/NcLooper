/*
 *		NCLOOPER		INTERACTIVE
 */

#include  <stdio.h>
#include  <unistd.h>

#include  "nclooper.h"

/* --------------------------------------------------------------------- */

extern int			verbosity;

/* --------------------------------------------------------------------- */
static int la_grande_boucle(SampleRef *psmpl, int notused)
{
static int		curpos = 0;
int			key;
int			flag_exit = 0;
char			line[120];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, psmpl, notused);
#endif

do	{

	sprintf(line, "curpos %4d", curpos);
	mvaddstr(1,50, line);

	refresh();

	/* - - - - handle human input, be careful */
	noecho();
	key = getch();
	echo();
	// sprintf(line, "   key %4d", key);
	// mvaddstr(2,50, line);

	switch(key) {
		case KEY_UP:
			if (curpos < 25) curpos++;
			break;
		case KEY_DOWN:
			if (curpos > 0)  curpos--;
			break;
		}		// end of swict


	flag_exit = ('X' == key);
	} while ( ! flag_exit );

return -1;
}
/* --------------------------------------------------------------------- */
int enter_interactive(SampleRef *psmpl, int notused)
{
int		foo, row, col;
char		txt[99];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, psmpl, notused);
#endif


for (foo=0; foo<26; foo++) {
	idx2position(foo, &row, &col);
	sprintf(txt, "%3d", foo);
	mvaddstr(row, col, txt);
	if (psmpl[foo].key) {
		standout();
		mvaddch(row, col+4, psmpl[foo].key);
		standend();
		}
	}
refresh();

foo = la_grande_boucle(psmpl, notused);		/* wtf ?
						   notused is used ? */

return -1;
}
/* --------------------------------------------------------------------- */
