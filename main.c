/*
 *      NcLooper        test des fonctions fichier (???)
 */
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <ctype.h>
#include  <ao/ao.h>

#include  "nclooper.h"

/* --------------------------------------------------------------------- */

int			verbosity;

SampleRef		the_samples[26];

/* --------------------------------------------------------------------- */
void help(int k)
{
puts("NcLooper : version pas finie...");
exit(0);
}
/* --------------------------------------------------------------------- */
/*
 *	all the basic engines are up, we have to fire ncurses
 */
int introduction(int k)
{
int			foo;
ao_device		*device;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, k);
#endif

foo = init_ecran(__func__);
sleep(1);

/* check the sound subsystem */


return -1;
}
/* --------------------------------------------------------------------- */
int start_the_engine(int k)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, k);
#endif

if (k) {
	fprintf(stderr, "%s: k %d is not null ?\n", __func__, k);
	exit(1);
	}

return -1;
}
/* --------------------------------------------------------------------- */
int conclusion(int k)
{
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, k);
#endif

foo = fin_ecran();

return -1;
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int			foo;
int			opt;
int			mode = 0;
char			*listname = "files/samples.list";

while ((opt = getopt(argc, argv, "hl:m:v")) != -1) {
	switch(opt) {
		case 'h':	help(0);			break;
		case 'l':	listname = optarg;		break;
		case 'm':	mode = atoi(optarg);		break;
		case 'v':	verbosity++;			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc = %d, optind = %d\n", argc, optind);
#endif

foo = lecture_liste_des_samples(listname, the_samples);
fprintf(stderr,"retour lecture liste '%s' -> %d\n", listname, foo);

foo = start_the_engine(mode);
fprintf(stderr,"retour start engine -> %d\n", foo);

introduction(0);

foo = enter_interactive(the_samples, 0);
#if DEBUG_LEVEL
fprintf(stderr, "retour 'enter interactive' -> %d\n", foo);
#endif

conclusion(0);

return 0;
}
/* --------------------------------------------------------------------- */

