/*
 *		NcLooper
 */


/* two arbitrary magic number */
#define   SZ_TEXT	20
#define   SZ_PATH	400

typedef struct	{
	char		key;
	int		flags;
	char		text[SZ_TEXT+1];
	char		path[SZ_PATH+1];
	} SampleRef;

#include  "files/smpllist.h"
#include  "files/ffuncs.h"

#include  "ui/ncfuncs.h"


int enter_interactive(SampleRef *, int notused);
