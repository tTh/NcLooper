
#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <ctype.h>

#include  "../nclooper.h"

#include  "ffuncs.h"
#include  "smpllist.h"

/* --------------------------------------------------------------------- */
extern int		verbosity;
/* --------------------------------------------------------------------- */
void affiche_un_sample(SampleRef *sref, int options)
{
printf("%c  %02X  [%-20s]  %s\n", sref->key, sref->flags, sref->text,
						sref->path);
}
/* --------------------------------------------------------------------- */
void affiche_liste_des_samples(SampleRef samples[])
{
int			foo;

for (foo=0; foo<26; foo++) {
	printf("%2d  %02x    %-30s %s\n", foo, samples[foo].key,
					samples[foo].text,
					samples[foo].path);
	}
}
/* --------------------------------------------------------------------- */
int decode_la_ligne(char *line, SampleRef *sref)
{
char			*ptr, *cp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, line, sref);
// dump(line);
#endif

/*
 *	ON THE WAY TO PARSING HELL
 */
ptr = strtok(line, "|");
if (NULL==ptr) {
	fprintf(stderr, "fubarized\n");
	return -1;
	}
cp = ltrim(rtrim(ptr));
if (verbosity) fprintf(stderr, "key   [%s]\n", cp);
if ( ! isalpha(*cp)) {
	fprintf(stderr, "invalid key 0x%02x\n", *cp);
	return -2;
	}
sref->key = toupper(*cp);

ptr = strtok(NULL, "|");
if (NULL==ptr) {
	fprintf(stderr, "line to short\n");
	return -3;
	}
cp = ltrim(rtrim(ptr));
if (verbosity) fprintf(stderr, "text  [%s]\n", cp);
if (strlen(cp) > SZ_TEXT) {
	fprintf(stderr, "text too long\n");
	return -4;
	}
strcpy(sref->text, cp);

ptr = strtok(NULL, "|");
if (NULL==ptr) {
	fprintf(stderr, "line to short\n");
	return -6;
	}
cp = ltrim(rtrim(ptr));
if (verbosity) fprintf(stderr, "path  [%s]\n", cp);
if (strlen(ptr) > SZ_PATH) {
	fprintf(stderr, "path too long\n");
	return -5;
	}
strcpy(sref->path, cp);

return 0;
}

/* --------------------------------------------------------------------- */
#define T_LINE		500

int lecture_liste_des_samples(char *fname, SampleRef *samples)
{
FILE			*fp;
char			line[T_LINE+1];
int			ln;				/* line number */
int			foo, bar, idx;
SampleRef		sample;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, fname);
#endif

/*
 *	first step : clean the current list
 */
for (foo=0; foo<26; foo++) {
	memset(samples+foo, 0, sizeof(SampleRef));
	}


if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	exit(1);
	}

ln = 1;
while (NULL != fgets(line, T_LINE, fp)) {

	/* degommer l'eventuel \n */
	bar = strlen(line);
	if (0==bar) {
		fprintf(stderr,"line %d very short\n", ln);
		continue;
		}
	bar--;			/* backspace one char */
	if ('\n' == line[bar]) line[bar]='\0';

	memset(&sample, 0, sizeof(SampleRef));
	foo = decode_la_ligne(line, &sample);
#if DEBUG_LEVEL > 1
	fprintf(stderr, "decode la ligne -> %d\n\n", foo);
#endif
	if (!foo) {
		affiche_un_sample(&sample, 0);
		idx = tolower(sample.key) - 'a';
		if (idx<0 || idx>25) {
			/* serious bug here */
			break;
			}
		memcpy(samples+idx, &sample, sizeof(SampleRef));
		}

	ln++;
	}

fclose(fp);

return -1;
}
/* --------------------------------------------------------------------- */
