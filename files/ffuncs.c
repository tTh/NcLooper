/*
 *		various functions
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <string.h>
#include  <ctype.h>

#include  "ffuncs.h"

/* --------------------------------------------------------------------- */

extern int			verbosity;

/* --------------------------------------------------------------------- */
void dump(unsigned char *ptr)
{
int		foo;

for (foo=0; foo<24; foo++) 	fprintf(stderr, "%02x ", ptr[foo]);
fputs("\n", stderr);
for (foo=0; foo<24; foo++) 	fprintf(stderr, "%c  ",
					isgraph(ptr[foo])?ptr[foo]:' ');
fputs("\n", stderr);

}
/* --------------------------------------------------------------------- */
char *rtrim(char *src)
{
int		foo;

for (foo=strlen(src)-1; src[foo]==' '|| src[foo]=='\t'; foo--) {
	src[foo] = '\0';
	}

return src;
}
/* --------------------------------------------------------------------- */
char *ltrim(char *src)
{
int		foo, bar;
char		*tmp;

tmp = alloca(strlen(src));

for (foo=0; src[foo]==' ' || src[foo]=='\t'; foo++);

bar = 0;
while (src[foo]!='\0') {
	tmp[bar]=src[foo];
	foo++;
	bar++;
	}

strcpy(src, tmp);

return src;
}
/* --------------------------------------------------------------------- */
