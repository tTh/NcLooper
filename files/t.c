/*
 *	small proggy for testing some functions
 */


#include  <stdio.h>
#include  <string.h>

#include  "ffuncs.h"

int verbosity = 1;

int test_of_the_trim_funcs(int foo)
{
char	buffer[200];

printf("     --- %s ---\n\n", __func__);

strcpy(buffer, "nothing to trim");
dump(buffer, NULL);
puts("");

strcpy(buffer, "trailing spaces    ");
dump(buffer, NULL);
rtrim(buffer);
dump(buffer, NULL);
puts("");

strcpy(buffer, "   leading spaces");
dump(buffer, NULL);
ltrim(buffer);
dump(buffer, NULL);
puts("");

return 0;
}

int main(int argc, char *argv[])
{

(void)test_of_the_trim_funcs(0);




return 0;
}
