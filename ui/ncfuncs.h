/*
 *	NcLooper--- interface curses, fonctions de base
 */

#include  <curses.h>

int init_ecran(const char *txt);
int fin_ecran(void);

int idx2position(int idx, int *prow, int *pcol);

