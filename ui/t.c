/*
 *      NcLooper        test des fonctions ncurses
 */
 
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  "../nclooper.h"
#include  "ncfuncs.h"

/* --------------------------------------------------------------------- */

int			verbosity;

/* --------------------------------------------------------------------- */
void help(int k)
{
puts("test des fonctions de l'interface utilisateur");
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int			foo, idx;
int			opt;
int			row, col;

while ((opt = getopt(argc, argv, "hv")) != -1) {
	switch(opt) {
		case 'h':	help(0);			break;
		case 'v':	verbosity++;			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc = %d, optind = %d\n", argc, optind);
#endif

foo = init_ecran("* NcLooper *");

for (idx=0; idx<26; idx++) {
	foo = idx2position(idx, &row, &col);
	standout();
	mvaddch(row, col,   '<');
	mvaddch(row, col+1, 'A' + idx);
	mvaddch(row, col+2, '>');
	standend();
	mvaddstr(row, col+5, "bla bla bla...");
	}
	refresh();

sleep(3);

fin_ecran();

return 0;
}
/* --------------------------------------------------------------------- */
