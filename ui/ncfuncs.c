/*
 *	NcLooper--- interface curses, fonctions de base
 */

#include <stdio.h>
#include <curses.h>

#include  "ncfuncs.h"

/* --------------------------------------------------------------------- */
int init_ecran(const char *txt)
{
initscr();

standout();
// border('o', 'o', 'o', 'o', 'X', 'X', 'X', 'X');
standend();
mvaddstr(1, 5, txt);
refresh();

keypad(stdscr, 1);

return -1;
}
/* --------------------------------------------------------------------- */
int fin_ecran(void)
{

endwin();

return 0;
}
/* --------------------------------------------------------------------- */
int idx2position(int idx, int *prow, int *pcol)
{

*prow = 6 + (idx % 13);
*pcol = 4 + (40*(idx/13));

return 0;
}
/* --------------------------------------------------------------------- */
