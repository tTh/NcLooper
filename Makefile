#
#	NC-LOOPER --- another kluge from tth
#

CC      = gcc
CCOPT   = -Wall -g -DDEBUG_LEVEL=0

OBJS	= ui/ncfuncs.o files/smpllist.o files/ffuncs.o
LIBS	= -lcurses

all:	nclooper

main.o:	main.c nclooper.h Makefile
	$(CC) ${CCOPT} -c $< 

interactive.o:	interactive.c nclooper.h Makefile
	$(CC) ${CCOPT} -c $< 

OBJS	+= interactive.o

nclooper:	main.o ${OBJS} Makefile
	$(CC) ${CCOPT} $< ${OBJS} ${LIBS} -o $@

